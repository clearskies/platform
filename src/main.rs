#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;
extern crate rocket_cors;
#[macro_use] extern crate serde_derive;

use rocket_contrib::Json;

#[derive(Deserialize)]
struct GenerateArgs {
    username: String
}

#[post("/generate", data = "<args>")]
fn generate(args: Json<GenerateArgs>) -> &'static str {
    println!("/generate with username: {}", args.username);
    "Hello, world!"
}

fn main() {
    let cors = rocket_cors::Cors {
        ..Default::default()
    };

    rocket::ignite()
        .mount("/", routes![generate])
        .attach(cors)
        .launch();
}